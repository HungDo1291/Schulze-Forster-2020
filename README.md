# Joint phoneme alignment and text-informed speech separation on highly corrupted speech


Here you find the code to reproduce the experiments of the paper "Joint phoneme alignment and text-informed speech separation on highly corrupted speech" by Kilian Schulze-Forster, Clement S. J. Doire, Gaël Richard, Roland Badeau. Accepted at *IEEE International Conference on Audio, Speech, and Signal Processing, 2020.*

The paper and audio examples are available [here](https://schufo.github.io/publications/2020-ICASSP)

## What I did:
<ul>
    <li>I set up the virtual environment as in the requirements.txt. Some library is deprecated so I have to look around for some alternative ways to install.</li>
    <li>I set up the MUSDB18 and TIMIT dataset and run the code to combine them into noisy monologues. Source: <a href="https://zenodo.org/record/3338373">MUSDB18</a> and <a href="https://figshare.com/articles/dataset/TIMIT_zip/5802597">TIMIT</a></li>
    <li>I trained and evaluated the model using the given code. To reduce the runtime, I only ran it on a Subset of the dataset (with torch.utils.data.Subset) for both training and evaluation.</li>
</ul>

